import { useState, useEffect, useRef, memo } from 'react'
import Box from '@mui/material/Box'
import LoadingButton from '@mui/lab/LoadingButton';
import { Button, Card, CardContent, Tooltip, Slider, Input, Collapse, Snackbar, FormControlLabel, Checkbox, Switch } from '@mui/material'
import { Container, CssBaseline, TextField, Typography, Grid, Divider } from '@mui/material'
import { io } from "socket.io-client";
import "./App.css"

// get web socket connection info from url before rendering anything
let urlParams = new URLSearchParams(window.location.search)
let host = urlParams.get('host')
let socketServerPort = urlParams.get('socketServerPort')
let fileServerPort = urlParams.get('fileServerPort')
console.log(host, socketServerPort, fileServerPort)

// different socket clients for different widgets
const inFileSocketA = io(`ws://${host}:${socketServerPort}`)
const inFileSocketB = io(`ws://${host}:${socketServerPort}`)
const runSocket = io(`ws://${host}:${socketServerPort}`)

function InputFieldA({text, updateOptsValue}) {
	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='input file' 
					onInput={(e) => {updateOptsValue('inputA', e.target.value)}}
					value={text}>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{inFileSocketA.emit('files')}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}

function InputFieldB({text, updateOptsValue}) {
	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='input file' 
					onInput={(e) => {updateOptsValue('inputB', e.target.value)}}
					value={text}>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{inFileSocketB.emit('files')}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}



function Title({text}) {
	return (
		<Typography variant='h5' sx={{margin: '20px'}}>
			{text}
		</Typography>
	)
}

function OutputField({text, updateOptsValue}) {
	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='output file'
					onInput={(e) => {updateOptsValue('-omat', e.target.value)}}
					value={text}
				>
				</TextField>
			</Grid>
		</Grid>
	)
}

function ActionButtons({commandString, isRunning, setIsRunning, options}) {
	return (
		<Grid container item xs={12} alignItems='center' justifyContent='center' spacing={0} direction='row'>
			<LoadingButton
				onClick={()=>{runSocket.emit('run', {'run': commandString}); setIsRunning(true)}}
        loading={isRunning}
        loadingPosition="end"
        variant="contained"
				style={{margin:0}}
      >
        Run
      </LoadingButton>
		</Grid>
	)
}

function CommandStringPreview({commandString}) {
	return (
		<Grid 
			container 
			item 
			xs={12}
			alignItems='center'
			justifyContent='center' 
			spacing={0}
			direction='row'
		>
			<Typography sx={{fontFamily: 'Monospace'}}>
				{`command: ${commandString}`}
			</Typography>
		</Grid>
	)
}

export default function ConcatXFM() {
	const title = "ConcatXFM"
  const defaultOpts = {
		'-omat': 			'output.mat',
		'inputA': 		'inputA.mat',
		'inputB': 		'inputB.mat',
	}
	const [opts, setOpts] = useState(defaultOpts)
	// the boolean variable to show or hide the snackbar
	const [showSnackBar, setShowSnackBar] = useState(false)
	// the message, and message setter for the snackbar
	const [snackBarMessage, setSnackBarMessage] = useState('')
	const [commandString, setCommandString] = useState('')
	const [isRunning, setIsRunning] = useState(false)
	
	if (host === null || socketServerPort === null || fileServerPort === null){
		setSnackBarMessage('unable to contact backend application')
	}

	inFileSocketA.on('files', (data) => {
		updateOptsValue('inputA', data[0] ? data[0] : '')
	})

	inFileSocketB.on('files', (data) => {
		updateOptsValue('inputB', data[0] ? data[0] : '')
	})


	runSocket.on('run', (data) => {
		console.log('run', data) 
		//let url = `http://${host}:${fileServerPort}/file/?filename=${betOpts['output']}`
		//let name = betOpts['output'].split('/').pop()
		if (isRunning){
			//addNiiVueImage(url)
		}
		setIsRunning(false)
	})

	function handleSnackBarClose() {
		setShowSnackBar(false)
	}

	function showSnackBarIfMsg() {
		if (snackBarMessage !== '') {
			setShowSnackBar(true)
		}
	}
	// whenever the snackbar message changes, run the effect (e.g. show the snackbar to the user when the message is updated)
	useEffect(() => {showSnackBarIfMsg()}, [snackBarMessage])
	useEffect(() => {updateCommandString()}, [opts])

	function setOutputName(inputA, inputB) {
		if (inputA === '' || typeof inputA === 'undefined') {
			return ''
		}
		if (inputB === '' || typeof inputB === 'undefined') {
			return ''
		}

		let extIdxA = inputA.lastIndexOf('.mat')
		let extIdxB = inputB.lastIndexOf('.mat')
		let pathA = inputA.substring(0,inputA.lastIndexOf('/'))
		let pathB = inputB.substring(0,inputB.lastIndexOf('/'))
		let basenameA = inputA.substring(inputA.lastIndexOf('/')+1, extIdxA)
		let basenameB = inputB.substring(inputB.lastIndexOf('/')+1, extIdxB)


		if (extIdxA < 0 || extIdxB < 0) {
			setSnackBarMessage('file must end in .mat')
			return ''
		} else {
			return `${pathB}/${basenameA}_to_${basenameB}.mat`
			//return fileName.substring(0, extIdx) + '_brain.nii.gz'
		}
	}

	function updateCommandString() {
		let command = 'convert_xfm '
		for (let [key, value] of Object.entries({...opts})) {
			if (value !== null) {
				if (value === false){
					continue
				} else if (value === true) {
					// if true the just pass the key to set boolean bet values
					value = ''	
				}
				command += `${(key=='inputA' || key=='inputB') ? '': key} ${(key==='-omat') ? `${value} -concat` : value} `
			}
		}
		setCommandString(command)
	}

	function updateOptsValue(option, value) {
		setOpts(defaultOpts => ({
			...defaultOpts,
			...{[option]: value}
		}))
		if (option === 'inputB') {
			setOpts(defaultOpts => ({
				...defaultOpts,
				...{'-omat': setOutputName(opts['inputA'], value)}
			}))
		}
		//updateCommandString()
	}

  return (
		<Container component="main" style={{height: '100%'}}>
			<CssBaseline enableColorScheme />
			<Box sx={{
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				height: '100%'
				}}>
				<Title text='ConcatXFM' />
				<Grid container spacing={2}>
					<InputFieldA
						updateOptsValue={updateOptsValue}
						text={opts['inputA']}
					/>
					<InputFieldB
						updateOptsValue={updateOptsValue}
						text={opts['inputB']}
					/>
					<OutputField 
						updateOptsValue={updateOptsValue}
						text={opts['-omat']}
					/>
					<ActionButtons
						commandString={commandString}
						isRunning={isRunning}
						setIsRunning={setIsRunning}
						options={opts}
					/>
					<CommandStringPreview commandString={commandString}/>
					<Snackbar
						anchorOrigin={{horizontal:'center', vertical:'bottom'}}
						open={showSnackBar}
						onClose={handleSnackBarClose}
						message={snackBarMessage}
						key='betSnackBar'
						autoHideDuration={5000}
					/>
				</Grid>
			</Box>
		</Container>
  )
}
